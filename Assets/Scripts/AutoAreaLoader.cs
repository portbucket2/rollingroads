﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAreaLoader : MonoBehaviour
{
    public int areaIndex;
    public AreaLoader loaderRef;

    public UnityEngine.UI.Button resetButton;
    void Start()
    {
        loaderRef.Load(areaIndex);

        resetButton.onClick.RemoveAllListeners();
        resetButton.onClick.AddListener(Reset);
    }

    private void Reset()
    {
        PlayerPrefs.DeleteAll();
        Application.LoadLevel(Application.loadedLevelName);
    }
}
