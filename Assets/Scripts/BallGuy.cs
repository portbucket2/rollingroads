﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGuy : MonoBehaviour
{
    public float currentMoveSpeed = .5f;
    public float moveSpeedMultiplier = 1;
    public float rotationSpeedMultiplier = 180;
    public BaseBlock currentBlock;
    public BaseBlock nextBlock
    {
        get
        {
            if (!currentBlock) return null;
            else return currentBlock.nextBlock;
        }
    }

    public GameObject colliderObj;
    public Rigidbody rgbd;

    public Transform ballViz;

    public float rotationSpeed
    {
        get
        {
            return currentMoveSpeed*rotationSpeedMultiplier*moveSpeedMultiplier;
        }
    }

    public static BallGuy instance;
    private void Awake()
    {
        instance = this;
        rgbd = this.GetComponent<Rigidbody>();
        ballisDead = false;
        Time.timeScale = 1;
    }


    float currentAngle;

    bool ballisDead;
    public event System.Action onDeath;
    private void Update()
    {
        if (this.transform.position.y < -3 && !ballisDead)
        {
            ballisDead = true;
            onDeath?.Invoke();
            MainGameManager.instance.runningLevelManager.OnComplete(false);
            return;

        }

        if (currentBlock != null)
        {


            MovementBudget budget = currentBlock.GetMovementBudget(this);
            if (budget != null) budget.UseForFrame(this);
            else colliderObj.SetActive(false);
        }

        float newAngle = currentAngle + (rotationSpeed * Time.deltaTime);
        currentAngle = newAngle % 360;
        ballViz.transform.localRotation = Quaternion.Euler(currentAngle,0,0);
        //ballViz.transform.Rotate(this.transform.right, rotationSpeed * Time.deltaTime,Space.Self);
    }

}
