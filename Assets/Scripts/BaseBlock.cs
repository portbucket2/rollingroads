﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBlock : MonoBehaviour
{
    public bool log;
    public Transform gateA;
    public Transform gateB;
    Transform inGate;
    Transform outGate;
    public List<Transform> directions = new List<Transform>();

    public List<Collider> triggerList = new List<Collider>();


    private void Start()
    {
        ResetPos();
    }
    public void ResetPos()
    {
        Vector3 pos = this.transform.position;
        pos.x = Mathf.RoundToInt(pos.x - 0.5f) + 0.5f;
        pos.z = Mathf.RoundToInt(pos.z - 0.5f) + 0.5f;
        this.transform.position = pos;
    }
    public void RotCW()
    {
        ResetPos();
        this.transform.Rotate(Vector3.up, 90, Space.World);
    }
    public void RotCA()
    {
        ResetPos();
        this.transform.Rotate(Vector3.up, -90, Space.World);
    }

    bool isTurnable;

    public void SetTurnable(bool turnable)
    {
        isTurnable = turnable;
    }

    public BaseBlock nextBlock;

    private BaseBlock GetNextBlock()
    {
        if (outGate != null)
        {
            RaycastHit rch;
            if (Physics.Raycast(this.transform.position, outGate.forward, out rch, 0.75f, LayerMask.GetMask("BLOCK"), QueryTriggerInteraction.Collide))
            {
                return rch.collider.GetComponentInParent<BaseBlock>();
            }
            Debug.DrawRay(this.transform.position, outGate.forward * 0.75f, Color.green, 2, depthTest: false);

            return null;
        }
        else
        {
            throw new System.Exception("Invalid Gate!");
        }
    }

    #region Registration
    int registrationCount = 0;
    private void OnRegister(BallGuy ballGuy)
    {
        if (registrationCount == 0)
        {
            lastBudget = null;
            ballGuy.currentBlock = this;
            Transform bgtr = ballGuy.transform;
            float distA = Vector3.Distance(bgtr.position, gateA.position);
            float distB = Vector3.Distance(bgtr.position, gateB.position);
            if (distA <= distB)
            {
                inGate = gateA;
                outGate = gateB;
            }
            else
            {
                inGate = gateB;
                outGate = gateA;
            }
            nextBlock = GetNextBlock();
            if (nextBlock) nextBlock.SetTurnable(true);
            this.SetTurnable(false);
//Debug.Log(nextBlock);
        }
        registrationCount++;
        if (log) Debug.Log("ball guy entered");
    }
    private void OnUnRegister(BallGuy ballGuy)
    {
        if (ballGuy.currentBlock == this)
        {
            if (registrationCount == 1)
            {
                ballGuy.currentBlock = null;
                //ballGuy.rgbd.useGravity = true;


                ballGuy.colliderObj.SetActive(false);
            }
            registrationCount--;
        }
        else
        {
            registrationCount = 0;
        }
        if (log) Debug.Log("ball guy exited");
    }
    private void OnTriggerEnter(Collider other)
    {
        BallGuy ballGuy = other.GetComponentInParent<BallGuy>();
        if (ballGuy != null)
        {
            OnRegister(ballGuy);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        BallGuy ballGuy = other.GetComponentInParent<BallGuy>();
        if (ballGuy != null)
        {
            OnUnRegister(ballGuy);
        }
    }
    #endregion

    #region Movement Budget
    private MovementBudget lastBudget;
    private int NextBudgetListIndex
    {
        get
        {
            if (lastBudget == null)
            {
                if (inGate == gateA)
                {
                    return 1;
                }
                else if (inGate == gateB)
                {
                    return directions.Count - 2;
                }
                else
                {
                    throw new System.Exception("Gate not recognized!");
                }
            }


            int currentIndex = -1;
            for (int i = 0; i < directions.Count; i++)
            {
                if (directions[i] == lastBudget.targetTransform)
                {
                    currentIndex = i;
                    break;
                }
            }
            if (currentIndex == -1)
            {
                throw new System.Exception("Invalid budget direction");
            }
            else
            {
                int nextIndex = -1;
                if (inGate == gateA)
                {
                    nextIndex = currentIndex + 1;
                }
                else if (inGate == gateB)
                {
                    nextIndex = currentIndex - 1;
                }
                else
                {
                    throw new System.Exception("Gate not recognized!");
                }
                //Debug.Log("Chosen Next Index" +nextIndex);
                if (nextIndex >= 0 && nextIndex < directions.Count) return nextIndex;
                else return -1;
            }
        }
    }

    public MovementBudget GetMovementBudget(BallGuy ballGuy)
    {
        if (lastBudget != null && !lastBudget.HasTimedOut) return lastBudget;
        int nextIndex = NextBudgetListIndex;
        if (nextIndex < 0)
        {
            return null;
        }
        else
        {
            //Debug.Log(this.gameObject.name);
            //Debug.Log(nextIndex);
            lastBudget = new MovementBudget(ballGuy, directions[nextIndex]);
            return lastBudget;
        }
    }
    #endregion


    #region Rotation Call
    public event System.Action onRotateRecieved;
    public void Rotate()
    {
        if (BallGuy.instance.currentBlock == this || isRotating) return;
        if (log) Debug.Log("Rotating");
        StartCoroutine(RotationRoutine());
        //this.transform.Rotate(Vector3.up, 90);

        onRotateRecieved?.Invoke();
    }

    bool isRotating;
    IEnumerator RotationRoutine()
    {
        isRotating = true;
        //foreach (var item in triggerList)
        //{
        //    item.enabled = false;
        //}
        //SetColor();
        float startTime = Time.time;
        float rotationTime = 0.15f;
        Vector3 initial = this.transform.forward;
        Vector3 final = this.transform.right;
        while (Time.time < startTime + rotationTime)
        {
            this.transform.forward = Vector3.Lerp(initial, final, Mathf.Clamp01((Time.time - startTime) / rotationTime));
            yield return null;
        }
        this.transform.forward = final;
        //foreach (var item in triggerList)
        //{
        //    item.enabled = true;
        //}
        isRotating = false;

        //SetColor();
    }
    #endregion

    #region Colors
    public List<MeshRenderer> rends;

    public Color normalCol;
    public Color onboardCol;

    public Material normalColorMaterial;
    public Material onboardColorMaterial;
    public Material rotatingColorMaterial;



    float lerpVal;
    bool posDir;


    public const float colorSpeed =2;
    private void Update()
    {
        if (BasicGameManager.isOnCamMode)
        {
            Color targetColor;
            if (isRotating)
            {
                targetColor = rotatingColorMaterial.color;
            }
            else
                targetColor = BallGuy.instance.currentBlock != this ? normalColorMaterial.color : onboardColorMaterial.color;
            foreach (var item in rends)
            {
                item.material.color = Color.Lerp(item.material.color, targetColor, 10f * Time.deltaTime);
            }
        }
        else
        {
            if (isTurnable)
            {
                if (posDir)
                {
                    lerpVal += colorSpeed * Time.deltaTime;
                    if (lerpVal >= 1)
                    {
                        posDir = false;
                        lerpVal = 1;
                    }
                }
                else
                {
                    lerpVal -= colorSpeed * Time.deltaTime;
                    if (lerpVal <= 0)
                    {
                        posDir = true;
                        lerpVal = 0;
                    }
                }
            }
            else
            {
                lerpVal -= colorSpeed * Time.deltaTime;
                if (lerpVal <= 0)
                {
                    posDir = true;
                    lerpVal = 0;
                }
            }

            Color selectedCol = Color.Lerp(normalCol, onboardCol, lerpVal);
            foreach (var item in rends)
            {
                item.material.color = selectedCol;
            }
        }
        



    }
    #endregion








}

public class MovementBudget
{
    public Vector3 initialForwardDirection;
    public Vector3 direction;
    public Transform targetTransform;
    public float startTime;
    public float endTime;
    public float RotationLerpCo { get { return Mathf.Clamp01((Time.time - startTime) / (endTime - startTime)); } }
    public Vector3 forwardNow
    {
        get
        {
            return Vector3.Lerp(initialForwardDirection, direction, RotationLerpCo);
        }
    }

    public bool HasTimedOut { get { return Time.time > endTime; } }

    public MovementBudget(BallGuy bg, Transform target)
    {
        float dist = Vector3.Distance(bg.transform.position, target.position);
        float timeBudget = dist / bg.currentMoveSpeed;
        startTime = Time.time;
        endTime = Time.time + timeBudget;
        targetTransform = target;
        direction = (target.transform.position - bg.transform.position).normalized;
        initialForwardDirection = bg.transform.forward;
        //Debug.Log(direction);
        // Debug.DrawLine(bg.transform.position, target.transform.position, Color.green, 2);

    }

    public void UseForFrame(BallGuy bg)
    {
        bg.transform.forward = forwardNow;
        bg.transform.Translate(direction * bg.currentMoveSpeed * bg.moveSpeedMultiplier * Time.deltaTime, Space.World);
    }
}
