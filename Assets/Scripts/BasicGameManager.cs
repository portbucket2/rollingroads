﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BasicGameManager : MonoBehaviour
{
    public const bool IS_CAMMODE_DEFAULT = true;
    FishSpace.HardData<bool> camMode;
    public static bool isOnCamMode
    {
        get
        {
            if (instance == null) return IS_CAMMODE_DEFAULT;
            return instance.camMode.value;
        }
        set
        {
            instance.camMode.value = value;
        }
    }
    static BasicGameManager instance;

    public Text modeButtText;
    public Image modeButtImage;
    void Start()
    {
        camMode = new FishSpace.HardData<bool>("mode", false);
        instance = this;
        modeButtText.text = string.Format("Mode ({0})", (isOnCamMode ? "Point" : "Free"));
        modeButtImage.color = (isOnCamMode ? Color.red : Color.cyan);
    }



    public void OnModeButton()
    {
        isOnCamMode = !isOnCamMode;

        modeButtText.text = string.Format("Mode ({0})", (isOnCamMode ? "Point" : "Free"));
        modeButtImage.color = (isOnCamMode ? Color.red : Color.cyan);
    }
    public void OnRestartButton()
    {
        Application.LoadLevel(Application.loadedLevelName);
    }
}
