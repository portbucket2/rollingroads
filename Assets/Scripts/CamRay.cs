﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamRay : MonoBehaviour
{
    Camera cam;
    public BallGuy guyRef;
    // Start is called before the first frame update
    void Start()
    {
        cam = this.GetComponent<Camera>();

    }

    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {
            if (BasicGameManager.isOnCamMode)
            {
                Ray ray = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                RaycastHit rch;
                if (Physics.Raycast(ray, out rch, 10,LayerMask.GetMask("BLOCK"),QueryTriggerInteraction.Collide))
                {
                    Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow, 2);

                    BaseBlock bb = rch.collider.gameObject.GetComponentInParent<BaseBlock>();

                    if (bb)
                    {
                        bb.Rotate();
                    }
                }
                else
                    Debug.DrawRay(ray.origin, ray.direction * 10, Color.red, 2);
            }
            else
            {
                if (guyRef.nextBlock) guyRef.nextBlock.Rotate();
            }

           
        }

    }
}
