﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamShot : MonoBehaviour
{
    public static int scCounter;
    public KeyCode captureKey = KeyCode.C;
    public string path;
    public int sizeMultiplier = 1;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(captureKey))
        {
            ScreenCapture.CaptureScreenshot(string.Format("{0}/{1}.png",path, scCounter++), sizeMultiplier);
        }
    }
}
