﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamTrack : MonoBehaviour
{
    public Transform ballGuyTransform
    {
        get
        {
            return BallGuy.instance.transform;
        }
    }
    // Start is called before the first frame update

    Vector3 relativePos;
    Transform targetObjectTransform;
    void Start()
    {

        targetObjectTransform = new GameObject("camTargetObject").transform;
        targetObjectTransform.position = this.transform.position;
        targetObjectTransform.rotation = this.transform.rotation;

        relativePos = targetObjectTransform.position - ballGuyTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        targetObjectTransform.position = ballGuyTransform.position + relativePos;
        this.transform.position = targetObjectTransform.position;
        //this.transform.position = Vector3.Lerp(this.transform.position, targetObjectTransform.position, 5f * Time.deltaTime);
    }
}
