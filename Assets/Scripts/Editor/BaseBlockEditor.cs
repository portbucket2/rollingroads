﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BaseBlock))]
public class BaseBlockEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BaseBlock myScript = (BaseBlock)target;
        if (GUILayout.Button("Clean Position"))
        {
            myScript.ResetPos();
        }

        if (GUILayout.Button("ClockWise"))
        {
            myScript.RotCW();
        }
        if (GUILayout.Button("ClockAnti"))
        {
            myScript.RotCA();
        }
    }
}
