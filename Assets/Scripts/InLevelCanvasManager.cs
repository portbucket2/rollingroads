﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InLevelCanvasManager : MonoBehaviour
{
    public GameObject successObj;
    public GameObject failureObj;

    public List<GameObject> stars;

    public Button nextButton;
    public Button restartButton;
    public Button replayButton;
    public Button levelsButton;



    public Button restartButtonExt;

    private LevelPrefabManager levelObject;
    public  void LevelStart(LevelPrefabManager levelObj)
    {
        levelObject = levelObj;
        failureObj.SetActive(false);
        successObj.SetActive(false);
        restartButtonExt.gameObject.SetActive(true);

        nextButton.onClick.RemoveAllListeners();
        nextButton.onClick.AddListener(levelObject.OnNext);

        restartButton.onClick.RemoveAllListeners();
        restartButton.onClick.AddListener(levelObject.OnReset);

        replayButton.onClick.RemoveAllListeners();
        replayButton.onClick.AddListener(levelObject.OnReset);

        levelsButton.onClick.RemoveAllListeners();
        levelsButton.onClick.AddListener(levelObject.OnLevels);


        restartButtonExt.onClick.RemoveAllListeners();
        restartButtonExt.onClick.AddListener(levelObject.OnReset);

        SetStars(0);
    }





    public void SetStars(int starCount)
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].SetActive(starCount > i);
        }
    }


    public void LoadFail()
    {
        failureObj.SetActive(true);
        restartButtonExt.gameObject.SetActive(false);
    }
    public void LoadSucces()
    {
        successObj.SetActive(true);
        restartButtonExt.gameObject.SetActive(false);
    }
}
