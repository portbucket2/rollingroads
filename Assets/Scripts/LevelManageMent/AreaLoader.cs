﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AreaLoader : MonoBehaviour
{
    public Text areaTitle;
    public GameObject levelUIPrefab;
    public RectTransform contentPanel;

    public event System.Action onRefresh;
    
    int areaIndex;
    private List<LevelUIItemLoader> levelitems = new List<LevelUIItemLoader>();
    public void Load(int areaIndex)
    {
        this.areaIndex = areaIndex;
        areaTitle.text = string.Format("Area {0}",areaIndex+1);
        LevelArea area = LevelLoader.instance.levelAreas[areaIndex];


        for (int i = 0; i < area.levelPrefs.Count; i++)
        {
            Transform tr = Instantiate(levelUIPrefab).transform;
            tr.SetParent(contentPanel);
            tr.localScale = Vector3.one;
            tr.rotation = Quaternion.identity;

            levelitems.Add(tr.GetComponent<LevelUIItemLoader>());
        }
        Refresh();
    }

    public void Refresh()
    {
        onRefresh?.Invoke();
        for (int i = 0; i < levelitems.Count; i++)
        {
            levelitems[i].Load(areaIndex, i);
        }
    }
}