﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;
using System;

public class LevelLoader : MonoBehaviour
{
    public List<LevelArea> levelAreas;

    public HardData<int> starCount;
    public HardData<int> areaIndex;
    public HardData<int> currentLevelIndex;

    public static LevelLoader instance;

    public bool allLevelsUnlocked=false;

    public void Awake()
    {
        if (instance)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.transform.root.gameObject);

            starCount = new HardData<int>("STAR", 0);
            areaIndex = new HardData<int>("AREA", 0);
            currentLevelIndex = new HardData<int>("LEVEL", 0);


            for (int a = 0; a < levelAreas.Count; a++)
            {
                LevelArea area = levelAreas[a];
                area.lastUnlockedLevelIndex = new HardData<int>(string.Format("UNLOCKED_PROGRSS_{0}", a), 0);
                area.stars = new List<HardData<int>>();
                for (int l = 0; l < area.levelPrefs.Count; l++)
                {
                    area.stars.Add(new HardData<int>(string.Format("stars_{0}_{1}", a, l), 0));
                }
            }
        }


    }

    public int GetStarFor(int areaIndex, int levelIndex)
    {
        return levelAreas[areaIndex].stars[levelIndex].value;
    }
    public void SetStarFor(int areaIndex, int levelIndex, int star)
    {
        levelAreas[areaIndex].stars[levelIndex].value = star;
    }
    public bool IsAreaLocked(int areaIndex)
    {
        return levelAreas[areaIndex].starToEnter > starCount.value;
    }
    public bool IsLevelLocked(int areaIndex, int levelIndex)
    {
        if (IsAreaLocked(areaIndex)) return true;
        else
        {
            return levelAreas[areaIndex].lastUnlockedLevelIndex.value < levelIndex;
        }
    }

}
[System.Serializable]
public class LevelArea
{
    public int starToEnter=0;
    public List<GameObject> levelPrefs;
    [NonSerialized]
    public List<HardData<int>> stars;
    [NonSerialized]
    public HardData<int> lastUnlockedLevelIndex;
} 