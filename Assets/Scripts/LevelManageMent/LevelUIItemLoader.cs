﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class LevelUIItemLoader : MonoBehaviour
{
    public Button mainButton;
    public GameObject lockImage;
    public GameObject playImage;
    public GameObject starModule;
    public List<GameObject> stars;
    public Text levelName;

    public void Load(int areaIndex, int levelIndex)
    {
        bool isUnlocked = LevelLoader.instance.allLevelsUnlocked || LevelLoader.instance.levelAreas[areaIndex].lastUnlockedLevelIndex.value >= levelIndex;
        levelName.text = string.Format("Level {0}", levelIndex+1);
        mainButton.interactable = isUnlocked;
        int starEarned = LevelLoader.instance.levelAreas[areaIndex].stars[levelIndex].value;
        lockImage.SetActive(!isUnlocked);
        starModule.SetActive(isUnlocked);
        playImage.SetActive(isUnlocked && starEarned <= 0);

        if (isUnlocked)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].SetActive(starEarned > i);
            }
        }


        mainButton.onClick.RemoveAllListeners();
        mainButton.onClick.AddListener(()=> { MainGameManager.instance.StartLevel(areaIndex,levelIndex); });
    }

   
}