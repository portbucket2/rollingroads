﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPrefabManager : MonoBehaviour
{
    int areaI;
    int levelI;

    int stars = 0;

    System.Action onEnd;

    public InLevelCanvasManager canvMan;

    public void Load(int areaIndex, int levelIndex, System.Action onEnd)
    {
        areaI = areaIndex;
        levelI = levelIndex;
        this.onEnd = onEnd;

        canvMan.LevelStart(this);
    }

    public void OnComplete(bool success)
    {
        if (success)
        {
            int starC = LevelLoader.instance.GetStarFor(areaI,levelI);
            if (starC >= stars)
            {
                stars = starC;
            }
            else
            {
                LevelLoader.instance.SetStarFor(areaI,levelI,stars);
            }
            if (LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value == levelI)
            {
                LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value++;
            }
            canvMan.LoadSucces();
        }
        else
        {
            canvMan.LoadFail();
        }
        canvMan.SetStars(stars);
    }

    public void AddStar()
    {
        stars++;
        canvMan.SetStars(stars);
    }

    public void OnNext()
    {
        onEnd();
    }
    public void OnReset()
    {
        MainGameManager.instance.StartLevel(areaI,levelI);
    }
    public void OnLevels()
    {
        onEnd();
    }
}
