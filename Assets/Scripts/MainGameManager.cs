﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
    public static MainGameManager instance;
    public GameObject loaderObjects;

    public AreaLoader areaLoader;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    public LevelPrefabManager runningLevelManager;


    public void StartLevel(int areaIndex,int levelIndex )
    {
        if (runningLevelManager)
        {
            EndLevel();
        }
        GameObject prefab = LevelLoader.instance.levelAreas[areaIndex].levelPrefs[levelIndex];
        runningLevelManager = Instantiate(prefab, Vector3.zero, Quaternion.identity).GetComponent<LevelPrefabManager>();
        runningLevelManager.Load(areaIndex,levelIndex, EndLevel);
        loaderObjects.SetActive(false);
    }
    public void EndLevel()
    {
        Destroy(runningLevelManager.gameObject);
        loaderObjects.SetActive(true);
        areaLoader.Refresh();
    }

}
