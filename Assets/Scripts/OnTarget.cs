﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTarget : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        BallGuy ballGuy = other.GetComponentInParent<BallGuy>();
        if (ballGuy != null)
        {
            ballGuy.moveSpeedMultiplier = 0;
            ballGuy.rgbd.useGravity = false;
            MainGameManager.instance.runningLevelManager.OnComplete(true);
        }
    }
}
