﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarController : MonoBehaviour
{
    bool offered = false;
    private void OnTriggerEnter(Collider other)
    {
        BallGuy ballGuy = other.GetComponentInParent<BallGuy>();
        if (ballGuy != null)
        {
            if (offered) return;
            else offered = true;
            this.GetComponent<Animator>().SetTrigger("collect");
            MainGameManager.instance.runningLevelManager.AddStar();
            FishSpace.Centralizer.Add_DelayedMonoAct(this,()=> { Destroy(this.gameObject);},2);
        }
    }
}
