﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggeredTutorial : MonoBehaviour
{

    public BaseBlock blockToRotate;
    public BallGuy ballGuy;

    public Text tutText;
    public GameObject arrowAnimated;
    // Start is called before the first frame update
    void Start()
    {
        blockToRotate.onRotateRecieved += OnComplete;
        ballGuy.onDeath += OnComplete;
    }

    bool tutorialOver;
    void OnComplete()
    {
        tutorialOver = true;

        StopAllCoroutines();
        blockToRotate.onRotateRecieved -= OnComplete;
        ballGuy.onDeath -= OnComplete;

        Time.timeScale = 1f;
        arrowAnimated.SetActive(false);
        tutText.gameObject.SetActive(false);

    }

    public float breakingTime = 0.5f;
    public float tutTimeScale = 0.1f;
    IEnumerator Tutorial()
    {
        float startTime = Time.realtimeSinceStartup;
        Color c = tutText.color;
        while (Time.realtimeSinceStartup < startTime + breakingTime)
        {
            float lerp = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / tutTimeScale);
            Time.timeScale = Mathf.Lerp(1, tutTimeScale, lerp);
            c.a = lerp;
            tutText.color=c;
            yield return null;
        }
        arrowAnimated.SetActive(true);
        c.a = 1;
        tutText.color = c;

        tutorialStarted = true;
        Debug.Log("Tutorial started");

        Time.timeScale = 0.05f;


    }

    bool tutorialStarted = false;
    private void OnTriggerEnter(Collider other)
    {
        if (tutorialOver) return;

        if (!tutorialStarted)
        {
            StopAllCoroutines();
            StartCoroutine(Tutorial());
        }
    }
}
